/*
 * =====================================================================================
 *
 *       Filename:  util.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_UTIL_H
#define MAXIMUM_DIVERSITY_PROBLEM_UTIL_H

#include <stdlib.h>     /* srand, rand */
#include <vector>
#include <climits>
#include <cfloat>
#include "representation/Problem.h"
#include "representation/Solution.h"
#include "representation/Point.h"

double sum(vector<double> vec);

int min(vector<double> cost);

Point getFarthestPoint(Point point, unordered_set<Point> problem);

Point getNearestPoint(Point point, unordered_set<Point> problem);

Point getOneOfFarthestPoints(Point point, unordered_set<Point> problem, int size);

Point getCenter(unordered_set<Point> points);

#endif //MAXIMUM_DIVERSITY_PROBLEM_UTIL_H
