/*
 * =====================================================================================
 *
 *       Filename:  Algorithm.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_ALGORITHM_H
#define MAXIMUM_DIVERSITY_PROBLEM_ALGORITHM_H

#include <string>

#include "representation/Problem.h"
#include "representation/Solution.h"

using namespace std;

class Algorithm {
private:
    string name = "MaximumDiversity";

public:
    Algorithm(string name) : name(name) {};

    virtual Solution solve(Problem problem, int size) = 0;

    virtual string getName(void) {
        return name;
    };
};

#endif //MAXIMUM_DIVERSITY_PROBLEM_ALGORITHM_H
