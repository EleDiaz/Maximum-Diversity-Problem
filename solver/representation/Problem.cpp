/*
 * =====================================================================================
 *
 *       Filename:  Problem.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  8/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#include "Problem.h"


Problem::Problem(istream &is): unordered_set<Point>() {
    int size;

    is >> size;
    is >> kSize;

    for (int i = 0; i < size; ++i) {
        Point point = Point(is, kSize);
        insert(point);
    }
}


Solution Problem::getSolution(int n) {
    Solution result;

    for (auto point : *this) {
        result.insert(point);

        if (--n== 0) {
            break;
        }
    }
    return result;
}

Problem::Problem() {

}



