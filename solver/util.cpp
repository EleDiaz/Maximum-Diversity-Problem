/*
 * =====================================================================================
 *
 *       Filename:  util.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include "util.h"


double sum(vector<double> vec) {
    double acc = 0;
    for (auto d : vec) {
        acc += d;
    }
    return acc;
}


int min(vector<double> cost) {
    int ix = 0;
    for (int i = 0; i < cost.size(); i++) {
        ix = cost[i] > cost[ix] ? ix : i;
    }
    return ix;
}


Point getFarthestPoint(Point point, unordered_set<Point> problem) {
    Point result;
    double distance = 0;

    for (auto point2 : problem) {
        double aux = point.distance(point2);
        if (aux > distance) {
            result = point2;
            distance = aux;
        }
    }
    return result;
}

Point getNearestPoint(Point point, unordered_set<Point> problem) {
    Point result;
    double distance = FLT_MAX;

    for (auto point2 : problem) {
        double aux = point.distance(point2);
        if (aux < distance) {
            result = point2;
            distance = aux;
        }
    }
    return result;
}


Point getOneOfFarthestPoints(Point point, unordered_set<Point> problem, int size) {
    vector<Point> rcl(size);
    vector<double> costRcl(size);

    for (auto point2 : problem) {
        double aux = point.distance(point2);
        int minIx = min(costRcl);
        if (costRcl[minIx] < aux) { // < <= ???
            costRcl[minIx] = aux;
            rcl[minIx] = point2;
        }
    }
    return rcl[rand() % size];
}

Point getCenter(unordered_set<Point> points) {
    Point result;

    for (auto point : points) {
        if (result.size() == 0) {
            result = point;
        }
        else {
            result += point;
        }
    }

    for (auto & comp : result) {
        comp = 1.0 / points.size() * comp;
    }
    return result;
}

