/*
 * =====================================================================================
 *
 *       Filename:  LocalSearch.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include "LocalSearch.h"


LocalSearch::LocalSearch(void): Algorithm("LocalSearch") {

}

Solution LocalSearch::solve(Problem p, int size) {
    Solution best = p.getSolution(size);
    double bestCost = best.cost();
    Problem aux;
    // Difference
    auto difference = [&] (Solution & sol) {
        for (auto point : sol) {
            aux.erase(point);
        }
    };

    bool improve;
    do {
        aux = p;
        difference(best);
        improve = false;

        // Interchanges
        for (auto point : best) {
            Solution current = best;
            current.erase(point);

            for (auto point2 : aux) {
                current.insert(point2);
                double currCost = current.cost();
                if (currCost > bestCost) {
                    bestCost = currCost;
                    best = current;
                    improve = true;
                }
                current.erase(point2);
            }
        }
    } while(improve);

    return best;
}


