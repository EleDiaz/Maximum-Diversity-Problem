/*
 * =====================================================================================
 *
 *       Filename:  Grasp.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_GRASP_H
#define MAXIMUM_DIVERSITY_PROBLEM_GRASP_H

#include "../Algorithm.h"
#include "../util.h"

class Grasp : public Algorithm {
private:
    int sizeRCL;

public:
    Grasp(int sizeRCL);

    virtual Solution solve(Problem problem, int size) override;
};


#endif //MAXIMUM_DIVERSITY_PROBLEM_GRASP_H
